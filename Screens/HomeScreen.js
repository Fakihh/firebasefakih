import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import React, {useContext, useState, useEffect} from 'react';
import FormButton from '../components/FormButton';
import {AuthContext} from '../navigation/AuthProvider';
import TouchID from 'react-native-touch-id';
import QrCode from './QrCode';



const HomeScreen = ({navigation}) => {
  const {user, logout} = useContext(AuthContext);
  const [isAuth, setIsAuth] = useState(false);

  const optionalConfigObject = {
    title: 'Masukan sidik jari anda', // Android
    imageColor: '#e00606', // Android
    imageErrorColor: '#ff0000', // Android
    sensorDescription: 'Touch sensor', // Android
    sensorErrorDescription: 'Failed', // Android
    cancelText: 'Cancel', // Android
    fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
    unifiedErrors: false, // use unified error messages (default false)
    passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
  };
  useEffect(() => {
    handleBiometric();
  });

  const handleBiometric = () => {
    TouchID.isSupported(optionalConfigObject).then(biometryType => {
      if (biometryType === 'FaceID') {
        console.log('FaceID is supported.');
      } else {
        console.log('TouchID is supported.');
        if (isAuth) {
          return null;
        }
        TouchID.authenticate('', optionalConfigObject)
          .then(success => {
            console.log('Sucess', success);
            setIsAuth(success);
          })
          .catch(err => {
            BackHandler.exitApp();
          });
      }
    });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Wellcome {user.uid}</Text>
      <FormButton
        buttonTitle="Barcode"
        onPress={() => navigation.navigate('QrCode')}
      />
      
      <FormButton buttonTitle="Logout" onPress={() => logout()} />
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f9fafd',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  text: {
    fontSize: 20,
    fontWeight:'bold',
    color: '#333333',
  },
});
